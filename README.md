## VITA2DLIB

Simple and Fast (using the GPU) 2D library for the PSVita

Install it by executing `./install-all.sh` of [vdpm](https://github.com/vitasdk/vdpm#readme).

对原版进行了一些修改，能够动态分配显存，显示中文不会再出现缺字问题。
修正了原版temp pool没有使用多缓存造成的屏闪问题。
修正了vita2d_texture初始化时没有置零导致free时的崩溃问题。
其它一些修改。
